<?php
require 'app/Models/Registro.php';
use Models\Registro;


class RegistroController
{
//Funcion para crear Producto
    function crearProducto()
    {
        $registro = new Registro();
        $registro->Nombre = $_POST['Nombre'];
        $registro->Caducidad1 = $_POST['Caducidad1'];
        $registro->Caducidad2 = $_POST['Caducidad2'];
        $registro->Descripcion = $_POST['Descripcion'];
        $registro->Cantidad = $_POST['Cantidad'];
        $registro->crearProducto();
        echo "Producto creado";
        echo "<pre>";
        print_r($registro);
        echo "</pre><hr>";
    }
    //Con esta funcion se mostrara  los registros
    function mostrarTodo(){
        $registro=Registro::mostrarRegistro();
        echo "<pre>";
        print_r($registro);
        echo "<pre><hr>";
    }
    //--**SABER CUANTOS productos SE REGISTARON EN "X" SEMANA Y cuales fueron**--\\

    function busquedaPorFecha(){
        //Variables a usar
        $Caducidad1=$_POST['fecha1'];
        $Caducidad2=$_POST['fecha2'];
        //Esta variable se usara como contador
        $cantidad=0;

        $registro=Registro::busquedaPorFecha($Caducidad1,$Caducidad2);
        //Si no hay resultados esta funcion manda un mensaje diciendo que no hay resultados
        if ($registro) {
            print_r($registro);

        }
    }
    //Funcion para eliminar una Producto
    function eliminarProducto(){

        $Nombre=$_POST['Nombre_Eliminar'];
        $eliminar=Registro::eliminarProducto($Nombre);
        echo "El producto ha sido eliminado ^-^";
    }
    //Con esta funcion se editara el producto
    function editarProducto(){
        $registro = new Registro();
        $registro->Nombre = $_POST['Nombre'];
        $registro->Caducidad1 = $_POST['Caducidad1'];
        $registro->Caducidad2 = $_POST['Caducidad2'];
        $registro->Descripcion = $_POST['Descripcion'];
        $registro->Cantidad = $_POST['Cantidad '];

        $registro->editarProducto();
        echo "Registro actualizado";
    }
}
