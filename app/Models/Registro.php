<?php



namespace Models;

require 'app/Models/Conexion.php';
class Registro extends Conexion
{
    //Variables que recibiran datos
    public $Nombre;
    public $Caducidad1;
    public $Caducidad2;
    public $Descripcion;
    public $Cantidad;

    //Contructor vacio
    function __construct()
    {
        parent::__construct();
    }
    //CROD para crear una Producto
    function crearProducto(){
        $pre = mysqli_prepare($this->con, "INSERT INTO producto(Nombre, Caducidad1, Caducidad2, Descripcion, Cantidad) VALUES (?,?,?,?,?)");
        $pre-> bind_param("sssss",$this->Nombre,$this->Caducidad1,$this->Caducidad2,$this->Descripcion,$this->Cantidad);
        $pre-> execute();
    }
    //Con esta consulta se muestran todos los Productos
    static function mostrarProductos(){
        $conexion = new Conexion();
        $pre= mysqli_prepare($conexion->con, "SELECT * FROM producto");
        $pre->execute();
        $resultado = $pre->get_result();
        while ($objeto=mysqli_fetch_assoc($resultado)) {
            $tarea[]=$objeto;
        }
        return $tarea;
    }
    //Funcion para mostrar Productos de acuerdo a "x" fecha
    static function busquedaPorFecha($dato1,$dato2){

        $conexion = new Conexion();
        $pre= mysqli_prepare($conexion->con, "SELECT * FROM producto WHERE Caducidad1 BETWEEN ? and ?");
        $pre-> bind_param("ss",$dato1,$dato2);
        $pre->execute();
        $resultado = $pre->get_result();
        $registro=[];
        while ($objeto=mysqli_fetch_assoc($resultado)) {
            $registro[]=$objeto;
        }
        return $registro;


}

//Con esto se hace la consulta para editar un producto por medio del nombre que recibe
    function editarProducto()
    {
        $pre = mysqli_prepare($this->con, "UPDATE producto SET Caducidad1=?,Caducidad2=?,Descripcion=?,Cantidad=? WHERE Nombre=? ");
        $pre-> bind_param("ssssss",$this->Caducidad1,$this->Caducidad2,$this->Descripcion,$this->Cantidad,$this->Nombre);
        $pre-> execute();
    }
    //Con esto se hace la consulta para eliminar un productopor medio del Nombre
    static function eliminarProducto($Nombre)
    {
        $conexion = new Conexion();
        $pre= mysqli_prepare($conexion->con, "DELETE FROM  producto WHERE Nombre=?");
        $pre->bind_param("s", $Nombre);
        $pre->execute();

    }




}